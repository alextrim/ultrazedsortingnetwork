library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.sorting.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DataRouter is
    Generic (
        WORDS_NUM : integer := 4
    );
    Port ( input_words : in WORD_ARRAY_type(WORDS_NUM-1 downto 0);
           ranks : in ARRAY_OF_SUM(WORDS_NUM-1 downto 0);
           output_words : out WORD_ARRAY_type(WORDS_NUM-1 downto 0);
           clk, rst : in STD_LOGIC);
end DataRouter;

architecture Behavioral of DataRouter is

signal output_words_buf : WORD_ARRAY_type(WORDS_NUM-1 downto 0);

begin

output_words <= output_words_buf;

process(rst, clk)
begin
    if(rst = '0') then
        output_words_buf <= (others => (others => '0'));
    elsif(rising_edge(clk)) then      
        for i in 0 to output_words_buf'LENGTH-1 loop
            output_words_buf(ranks(i)) <= input_words(i);
        end loop;
    end if;
end process;

end Behavioral;