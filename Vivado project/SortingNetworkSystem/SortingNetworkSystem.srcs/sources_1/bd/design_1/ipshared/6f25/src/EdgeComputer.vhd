library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.sorting.all;

entity EdgeComputer is
    Generic (
        WORDS_NUM : integer := 4
    );
    Port ( input_words : in WORD_ARRAY_type(WORDS_NUM-1 downto 0);
           output_matrix : out STD_LOGIC_VECTOR(((WORDS_NUM*(WORDS_NUM-1))/2)-1 downto 0));
end EdgeComputer;

architecture Behavioral of EdgeComputer is
begin

process(input_words)
variable cnt : integer range 0 to ((WORDS_NUM*(WORDS_NUM-1))/2);
begin
    cnt := 0;
    for i in 1 to WORDS_NUM-1 loop
        for j in 0 to WORDS_NUM-(WORDS_NUM-i)-1 loop
            if(input_words(i) < input_words(j)) then
                output_matrix(cnt) <= '1';
            else
                output_matrix(cnt) <= '0';
            end if;
            cnt := cnt + 1;
        end loop;
    end loop;

end process;

end Behavioral;