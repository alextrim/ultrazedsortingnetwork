----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.04.2020 14:15:29
-- Design Name: 
-- Module Name: DReg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DReg is
    generic (
        word_size : integer   := 32
    );
    port (
        clk : in std_logic;
     
        resetn : in std_logic;
        en : in std_logic;
        
        data_in : in std_logic_vector(word_size-1 downto 0);
        data_out : out std_logic_vector(word_size-1 downto 0)
    );
end DReg;

architecture Behavioral of DReg is

begin

    process(clk)
	begin
	   if(rising_edge(clk)) then
	       if(resetn = '0') then
	           data_out <= (others => '0');
	       elsif(en = '1') then
               data_out <= data_in;
	       end if;
	   end if;
	end process;

end Behavioral;
