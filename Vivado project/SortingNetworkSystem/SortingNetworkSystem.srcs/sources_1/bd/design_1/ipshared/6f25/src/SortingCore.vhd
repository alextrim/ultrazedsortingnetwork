library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.sorting.all;

entity SortingCore is
    Generic (
        WORDS_NUM : integer := 4
    );
    Port (
        DATA_INPUT : in WORD_ARRAY_type(0 to WORDS_NUM-1);
        DATA_OUTPUT : out WORD_ARRAY_type(0 to WORDS_NUM-1);
        clk, rst : in STD_LOGIC
    );
end SortingCore;

architecture Behavioral of SortingCore is

    signal matrix : STD_LOGIC_VECTOR(((WORDS_NUM*(WORDS_NUM-1))/2)-1 downto 0);
    signal ranks : ARRAY_OF_SUM(WORDS_NUM-1 downto 0);

    component EdgeComputer is
        Generic (
            WORDS_NUM : integer := 4
        );
        Port ( input_words : in WORD_ARRAY_type(WORDS_NUM-1 downto 0);
               output_matrix : out STD_LOGIC_VECTOR(((WORDS_NUM*(WORDS_NUM-1))/2)-1 downto 0));
    end component;
    
    component RankComputer is
        Generic (
            WORDS_NUM : integer := 4
        );
        Port ( input_matrix : in STD_LOGIC_VECTOR(((WORDS_NUM*(WORDS_NUM-1))/2)-1 downto 0);
               output_sums : out ARRAY_OF_SUM(WORDS_NUM-1 downto 0);
               clk, rst : in STD_LOGIC);
    end component;
    
    component DataRouter is
        Generic (
            WORDS_NUM : integer := 4
        );
        Port ( input_words : in WORD_ARRAY_type(WORDS_NUM-1 downto 0);
               ranks : in ARRAY_OF_SUM(WORDS_NUM-1 downto 0);
               output_words : out WORD_ARRAY_type(WORDS_NUM-1 downto 0);
               clk, rst : in STD_LOGIC);
    end component;

begin

    EDGECOMP : EdgeComputer generic map (WORDS_NUM) port map (input_words => DATA_INPUT, output_matrix => matrix);
    RANGCOMP : RankComputer generic map (WORDS_NUM) port map (input_matrix => matrix, output_sums => ranks, clk => clk, rst => rst);
    DATAROUT : DataRouter generic map (WORDS_NUM) port map (input_words => DATA_INPUT, ranks => ranks, output_words => DATA_OUTPUT, clk => clk, rst => rst);

end Behavioral;
