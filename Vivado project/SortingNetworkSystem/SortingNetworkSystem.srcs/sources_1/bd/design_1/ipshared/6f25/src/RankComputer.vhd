library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.sorting.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RankComputer is
    Generic (
        WORDS_NUM : integer := 4
    );
    Port ( input_matrix : in STD_LOGIC_VECTOR(((WORDS_NUM*(WORDS_NUM-1))/2)-1 downto 0);
           output_sums : out ARRAY_OF_SUM(WORDS_NUM-1 downto 0);
           clk, rst : in STD_LOGIC);
end RankComputer;

architecture Behavioral of RankComputer is

    type WORD_POLARITY_type is array (integer range <>) of std_logic;
    type WORD_POINTERS_type is array (integer range <>) of natural range 0 to ((WORDS_NUM*(WORDS_NUM-1))/2)-1;
    type POINTERS_type is array (integer range <>) of WORD_POINTERS_type(WORDS_NUM-2 downto 0);
    type POINTERS_POLARITY_type is array (integer range <>) of WORD_POLARITY_type(WORDS_NUM-2 downto 0);

    function initPointers return POINTERS_type is
        variable tmp : POINTERS_type(WORDS_NUM-1 downto 0);
        variable cnt : natural range 0 to ((WORDS_NUM*(WORDS_NUM-1))/2)-1;
    begin
        for i in 0 to WORDS_NUM-1 loop
            cnt := 0;
            for j in 0 to WORDS_NUM-1 loop
                if(j < i) then
                    tmp(i)(cnt) := i*(i-1)/2 + j;
                    cnt := cnt + 1;
                elsif(j > i) then
                    tmp(i)(cnt) := j*(j-1)/2 + i;
                    cnt := cnt + 1;
                end if;
            end loop;
        end loop;
        return tmp;
    end function initPointers;

    function initPolarity return POINTERS_POLARITY_type is
        variable tmp : POINTERS_POLARITY_type(WORDS_NUM-1 downto 0);
        variable cnt : natural range 0 to ((WORDS_NUM*(WORDS_NUM-1))/2)-1;
    begin
        for i in 0 to WORDS_NUM-1 loop
            cnt := 0;
            for j in 0 to WORDS_NUM-1 loop
                if(j < i) then
                    tmp(i)(cnt) := '0';
                    cnt := cnt + 1;
                elsif(j > i) then
                    tmp(i)(cnt) := '1';
                    cnt := cnt + 1;
                end if;
            end loop;
        end loop;
        return tmp;
    end function initPolarity;
    
    --signal busy : std_logic;
    signal ptrs : POINTERS_type(WORDS_NUM-1 downto 0) := initPointers;
    signal ptrs_pol : POINTERS_POLARITY_type(WORDS_NUM-1 downto 0) := initPolarity;

begin

process(rst, clk)
    variable cnt : ARRAY_OF_SUM(WORDS_NUM-1 downto 0);
    variable matrix_val : std_logic;
    variable polarity : std_logic;
begin
    if(rst = '0') then
        output_sums <= (others => 0);
        cnt := (others => 0);
    elsif(rising_edge(clk)) then
            for i in 0 to WORDS_NUM-1 loop
                cnt(i) := 0;
                for j in 0 to WORDS_NUM-2 loop
                    matrix_val := input_matrix(ptrs(i)(j));
                    polarity := ptrs_pol(i)(j);
                    if((matrix_val = '1' and polarity = '0') or (matrix_val = '0' and polarity = '1')) then
                        cnt(i) := cnt(i) + 1;
                    end if;
                end loop;
            end loop;
            output_sums <= cnt;
    end if;
end process;

end Behavioral;