----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.04.2020 13:37:12
-- Design Name: 
-- Module Name: ShiftReg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.sorting.all;

entity ShiftReg is
    generic (
        depth : integer   := 8
    );
    port (
        clk : in std_logic;
     
        resetn : in std_logic;
        en : in std_logic;
        full : out std_logic;
        
        data_in : in std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0);
        dataArray : out WORD_ARRAY_type(0 to depth-1)
    );
end ShiftReg;

architecture Behavioral of ShiftReg is

signal data : WORD_ARRAY_type(0 to depth-1);
signal dataZeros : std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0) := (others => '0');
signal full_buf : std_logic;
signal counter : natural range 0 to depth;

component DReg is
    generic (
        word_size : integer   := 32
    );
    port (
        clk : in std_logic;
     
        resetn : in std_logic;
        en : in std_logic;
        
        data_in : in std_logic_vector(word_size-1 downto 0);
        data_out : out std_logic_vector(word_size-1 downto 0)
    );
end component DReg;

begin

    dataArray <= data;
    full <= full_buf;

    reg_0 : DReg generic map (C_AXIS_TDATA_WIDTH) port map(clk => clk, resetn => resetn, en => en, data_in => data_in, data_out => data(0));

    GEN_REG:
    for i in 1 to depth-1 generate
        reg_0 : DReg generic map (C_AXIS_TDATA_WIDTH) port map(clk => clk, resetn => resetn, en => en, data_in => data(i-1), data_out => data(i));
    end generate GEN_REG;

    full_buf <= '1' when counter = depth else '0';
    
    process(clk)
    begin
	   if(rising_edge(clk)) then
	       if(resetn = '0') then
	           counter <= 0;
	       else
               if(en = '1' and full_buf = '0') then
                   counter <= counter + 1;
               end if;
	       end if;
	   end if;
    end process;

end Behavioral;
