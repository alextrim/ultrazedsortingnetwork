----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.03.2020 20:05:48
-- Design Name: 
-- Module Name: Sorting_TB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.sorting.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Sorting_TB is
--  Port ( );
end Sorting_TB;

architecture Behavioral of Sorting_TB is

    constant PERIOD: time := 100 ns;
    constant C_AXIS_DATA_NUM : integer := 4;

    signal DATA : WORD_ARRAY_type(0 to C_AXIS_DATA_NUM-1);
    signal RESULT : WORD_ARRAY_type(0 to C_AXIS_DATA_NUM-1);
    signal clk, rst : std_logic;
    
    component SortingCore is
        Generic (
            WORDS_NUM : integer := 4
        );
        Port (
            DATA_INPUT : in WORD_ARRAY_type(0 to WORDS_NUM-1);
            DATA_OUTPUT : out WORD_ARRAY_type(0 to WORDS_NUM-1);
            clk, rst : in STD_LOGIC
        );
    end component SortingCore;

begin

	SortingCore_inst : SortingCore
	generic map (
	    WORDS_NUM => C_AXIS_DATA_NUM
	)
	port map (
		DATA_INPUT	=> DATA,
		DATA_OUTPUT	=> RESULT,
		clk	=> clk,
		rst	=> rst
	);
	
clk_generation :process
    begin
        clk <= '1';
        wait for PERIOD / 2;
        clk <= '0';
        wait for PERIOD / 2;
    end process clk_generation;

simulation: process
    begin
        DATA(0) <= x"02";
        DATA(1) <= x"07";
        DATA(2) <= x"00";
        DATA(3) <= x"04";
        rst <= '0';
        wait for 300ns;
        rst <= '1';
        wait until clk = '1';
        wait until clk = '0';
--        DATA(4) <= x"33333333";
--        DATA(5) <= x"99999999";
--        DATA(6) <= x"45454545";
--        DATA(7) <= x"44444444";
        
        wait;     
    end process simulation;

end Behavioral;
