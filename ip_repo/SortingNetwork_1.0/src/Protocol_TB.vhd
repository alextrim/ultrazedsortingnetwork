library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.sorting.all;

entity Protocol_TB is
--  Port ( );
end Protocol_TB;

architecture Behavioral of Protocol_TB is

component SotringModule_v1_0 is
	generic (
        C_AXIS_TOTAL_DATA_NUM       : integer   := 8;
        C_AXIS_TRANSFER_DATA_NUM    : integer   := 1
	);
	port (
		s00_axis_aclk	: in std_logic;
		s00_axis_aresetn	: in std_logic;
		s00_axis_tready	: out std_logic;
		s00_axis_tdata	: in std_logic_vector(C_AXIS_TDATA_WIDTH*C_AXIS_TRANSFER_DATA_NUM-1 downto 0);
		s00_axis_tstrb	: in std_logic_vector(((C_AXIS_TDATA_WIDTH*C_AXIS_TRANSFER_DATA_NUM)/8)-1 downto 0);
		s00_axis_tlast	: in std_logic;
		s00_axis_tvalid	: in std_logic;

		m00_axis_aclk	: in std_logic;
		m00_axis_aresetn	: in std_logic;
		m00_axis_tvalid	: out std_logic;
		m00_axis_tdata	: out std_logic_vector(C_AXIS_TDATA_WIDTH*C_AXIS_TRANSFER_DATA_NUM-1 downto 0);
		m00_axis_tstrb	: out std_logic_vector(((C_AXIS_TDATA_WIDTH*C_AXIS_TRANSFER_DATA_NUM)/8)-1 downto 0);
		m00_axis_tlast	: out std_logic;
		m00_axis_tready	: in std_logic;
		
        debug_data1	: out std_logic;
		debug_data2	: out std_logic;
		debug_done : out std_logic;
		m00_state : out std_logic_vector(1 downto 0)
	);
end component;

signal  axis_aclk     : std_logic;
signal	axis_aresetn  : std_logic;

signal	s_axis_tready	: std_logic;
signal	s_axis_tdata	: std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0) := (others => '0');
signal	s_axis_tstrb	: std_logic_vector((C_AXIS_TDATA_WIDTH/8)-1 downto 0) := (others => '0');
signal	s_axis_tlast	: std_logic := '0';
signal	s_axis_tvalid	: std_logic := '0';


signal	m_axis_tvalid	: std_logic;
signal	m_axis_tdata	: std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0);
signal	m_axis_tstrb	: std_logic_vector((C_AXIS_TDATA_WIDTH/8)-1 downto 0);
signal	m_axis_tlast	: std_logic;
signal	m_axis_tready	: std_logic := '0';

constant PERIOD: time := 100 ns;
constant TOTAL_DATA_NUM: integer := 8;
constant TRANSFER_DATA_NUM: integer := 1;

begin

SotringModule_v1_0_inst : SotringModule_v1_0
	generic map (
		C_AXIS_TOTAL_DATA_NUM  => TOTAL_DATA_NUM,
		C_AXIS_TRANSFER_DATA_NUM => TRANSFER_DATA_NUM
	)
	port map (
		s00_axis_aclk	 => axis_aclk,
		s00_axis_aresetn => axis_aresetn,
		s00_axis_tready	 => s_axis_tready,
		s00_axis_tdata	 => s_axis_tdata,
		s00_axis_tstrb	 => s_axis_tstrb,
		s00_axis_tlast	 => s_axis_tlast,
		s00_axis_tvalid	 => s_axis_tvalid,

		m00_axis_aclk	 => axis_aclk,
		m00_axis_aresetn => axis_aresetn,
		m00_axis_tvalid	 => m_axis_tvalid,
		m00_axis_tdata	 => m_axis_tdata,
		m00_axis_tstrb	 => m_axis_tstrb,
		m00_axis_tlast	 => m_axis_tlast,
		m00_axis_tready	 => m_axis_tready,
        debug_data1 => open,
		debug_data2 => open,
		debug_done => open,
		m00_state => open
	);
	
clk_generation :process
    begin
        axis_aclk <= '1';
        wait for PERIOD / 2;
        axis_aclk <= '0';
        wait for PERIOD / 2;
    end process clk_generation;
    
simulation: process
    begin
        axis_aresetn <= '0';
        wait for 300ns;
        axis_aresetn <= '1';
        m_axis_tready <= '1';
        wait until axis_aclk = '0';
        wait until axis_aclk = '1';
        wait until axis_aclk = '0';
        
--        for i in 0 to 6 loop
--            s_axis_tdata <= x"12345678";
--            s_axis_tvalid <= '1';
--            wait until axis_aclk = '1';
--            wait until axis_aclk = '0';
--        end loop;

            s_axis_tdata <= x"11111111";
            s_axis_tvalid <= '1';
            wait until s_axis_tready = '1';
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
            
            s_axis_tdata <= x"22222222";
            s_axis_tvalid <= '1';
            while s_axis_tready = '0' loop
                wait until axis_aclk = '1';
                wait until axis_aclk = '0';
            end loop;
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
            
            s_axis_tdata <= x"33333333";
            s_axis_tvalid <= '1';
            while s_axis_tready = '0' loop
                wait until axis_aclk = '1';
                wait until axis_aclk = '0';
            end loop;
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
            
            s_axis_tdata <= x"44444444";
            s_axis_tvalid <= '1';
            while s_axis_tready = '0' loop
                wait until axis_aclk = '1';
                wait until axis_aclk = '0';
            end loop;
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
            
            s_axis_tdata <= x"55555555";
            s_axis_tvalid <= '1';
            while s_axis_tready = '0' loop
                wait until axis_aclk = '1';
                wait until axis_aclk = '0';
            end loop;
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
            
            s_axis_tdata <= x"66666666";
            s_axis_tvalid <= '1';
            while s_axis_tready = '0' loop
                wait until axis_aclk = '1';
                wait until axis_aclk = '0';
            end loop;
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
            
            s_axis_tdata <= x"77777777";
            s_axis_tvalid <= '1';
            while s_axis_tready = '0' loop
                wait until axis_aclk = '1';
                wait until axis_aclk = '0';
            end loop;
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
            
            s_axis_tdata <= x"88888888";
            s_axis_tvalid <= '1';
            while s_axis_tready = '0' loop
                wait until axis_aclk = '1';
                wait until axis_aclk = '0';
            end loop;
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
            
            s_axis_tdata <= x"99999999";
            s_axis_tvalid <= '1';
            while s_axis_tready = '0' loop
                wait until axis_aclk = '1';
                wait until axis_aclk = '0';
            end loop;
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
        
            s_axis_tdata <= x"10101010";
            s_axis_tvalid <= '1';
            s_axis_tlast <= '1';
            while s_axis_tready = '0' loop
                wait until axis_aclk = '1';
                wait until axis_aclk = '0';
            end loop;
            
            wait until axis_aclk = '1';
            wait until axis_aclk = '0';
            s_axis_tvalid <= '0';
            
        wait until axis_aclk = '1';
        wait until axis_aclk = '0';

        wait for 100ns;
        wait until axis_aclk = '0';
        wait until axis_aclk = '1';
        wait;

    end process simulation;

end Behavioral;
