library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package sorting is
    constant C_AXIS_TDATA_WIDTH : natural := 32;
    type WORD_ARRAY_type is array (natural range <>) of std_logic_vector (C_AXIS_TDATA_WIDTH-1 downto 0);
    type ARRAY_OF_SUM is array (natural range <>) of natural;
end sorting;