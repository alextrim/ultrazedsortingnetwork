library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.sorting.all;

entity SortingNetwork_v1_0 is
	generic (
		-- Users to add parameters here
        C_AXIS_NUM_OF_PACKETS           : integer   := 8
		-- User parameters ends
		-- Do not modify the parameters beyond this line 
	);
	port (
        -- Ports of Axi Slave Bus Interface S00_AXIS
		s00_axis_aclk	: in std_logic;
		s00_axis_aresetn	: in std_logic;
		s00_axis_tready	: out std_logic;
		s00_axis_tdata	: in std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0);
		s00_axis_tstrb	: in std_logic_vector(((C_AXIS_TDATA_WIDTH)/8)-1 downto 0);
		s00_axis_tlast	: in std_logic;
		s00_axis_tvalid	: in std_logic;

		-- Ports of Axi Master Bus Interface M00_AXIS
		m00_axis_aclk	: in std_logic;
		m00_axis_aresetn	: in std_logic;
		m00_axis_tvalid	: out std_logic;
		m00_axis_tdata	: out std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0);
		m00_axis_tstrb	: out std_logic_vector(((C_AXIS_TDATA_WIDTH)/8)-1 downto 0);
		m00_axis_tlast	: out std_logic;
		m00_axis_tready	: in std_logic
	);
end SortingNetwork_v1_0;

architecture arch_imp of SortingNetwork_v1_0 is

    signal DATA : WORD_ARRAY_type(0 to (C_AXIS_NUM_OF_PACKETS)-1);
    signal RESULT : WORD_ARRAY_type(0 to (C_AXIS_NUM_OF_PACKETS)-1);
    signal NEW_DATA_READY : std_logic;
    signal DATA_TRANSMITTED : std_logic;

	-- component declaration
	component SortingNetwork_v1_0_S00_AXIS is
		generic (
		C_S_AXIS_NUM_OF_PACKETS           : integer   := 8
		);
		port (
        S_STREAM_DATA : out WORD_ARRAY_type(0 to (C_S_AXIS_NUM_OF_PACKETS)-1);
        S_NEW_DATA_READY : out std_logic;
        S_DATA_TRANSMITTED : in std_logic;

		S_AXIS_ACLK	: in std_logic;
		S_AXIS_ARESETN	: in std_logic;
		S_AXIS_TREADY	: out std_logic;
		S_AXIS_TDATA	: in std_logic_vector((C_AXIS_TDATA_WIDTH)-1 downto 0);
		S_AXIS_TSTRB	: in std_logic_vector(((C_AXIS_TDATA_WIDTH)/8)-1 downto 0);
		S_AXIS_TLAST	: in std_logic;
		S_AXIS_TVALID	: in std_logic
		);
	end component SortingNetwork_v1_0_S00_AXIS;

	component SortingNetwork_v1_0_M00_AXIS is
		generic (
        C_M_AXIS_NUM_OF_PACKETS           : integer   := 8
		);
		port (
        M_STREAM_DATA : in WORD_ARRAY_type(0 to (C_M_AXIS_NUM_OF_PACKETS)-1);
        M_NEW_DATA_READY : in std_logic;
        M_DATA_TRANSMITTED : out std_logic;
        
		M_AXIS_ACLK	: in std_logic;
		M_AXIS_ARESETN	: in std_logic;
		M_AXIS_TVALID	: out std_logic;
		M_AXIS_TDATA	: out std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0);
		M_AXIS_TSTRB	: out std_logic_vector(((C_AXIS_TDATA_WIDTH)/8)-1 downto 0);
		M_AXIS_TLAST	: out std_logic;
		M_AXIS_TREADY	: in std_logic
		);
	end component SortingNetwork_v1_0_M00_AXIS;
	
	component SortingCore is
        Generic (
            WORDS_NUM : integer := 4
        );
        Port (
            DATA_INPUT : in WORD_ARRAY_type(0 to WORDS_NUM-1);
            DATA_OUTPUT : out WORD_ARRAY_type(0 to WORDS_NUM-1);
            clk, rst : in STD_LOGIC
        );
    end component SortingCore;

begin

-- Instantiation of Axi Bus Interface S00_AXIS
SortingNetwork_v1_0_S00_AXIS_inst : SortingNetwork_v1_0_S00_AXIS
	generic map (
		C_S_AXIS_NUM_OF_PACKETS	=> C_AXIS_NUM_OF_PACKETS
	)
	port map (
	    S_STREAM_DATA => DATA,
	    S_NEW_DATA_READY => NEW_DATA_READY,
        S_DATA_TRANSMITTED => DATA_TRANSMITTED,
        
		S_AXIS_ACLK	=> s00_axis_aclk,
		S_AXIS_ARESETN	=> s00_axis_aresetn,
		S_AXIS_TREADY	=> s00_axis_tready,
		S_AXIS_TDATA	=> s00_axis_tdata,
		S_AXIS_TSTRB	=> s00_axis_tstrb,
		S_AXIS_TLAST	=> s00_axis_tlast,
		S_AXIS_TVALID	=> s00_axis_tvalid
	);

-- Instantiation of Axi Bus Interface M00_AXIS
SortingNetwork_v1_0_M00_AXIS_inst : SortingNetwork_v1_0_M00_AXIS
	generic map (
        C_M_AXIS_NUM_OF_PACKETS	=> C_AXIS_NUM_OF_PACKETS
	)
	port map (
        M_STREAM_DATA => RESULT,
        M_NEW_DATA_READY => NEW_DATA_READY,
        M_DATA_TRANSMITTED => DATA_TRANSMITTED,
--        M_STATE => m00_state,
--        M_DONE => debug_done,
        
		M_AXIS_ACLK	=> m00_axis_aclk,
		M_AXIS_ARESETN	=> m00_axis_aresetn,
		M_AXIS_TVALID	=> m00_axis_tvalid,
		M_AXIS_TDATA	=> m00_axis_tdata,
		M_AXIS_TSTRB	=> m00_axis_tstrb,
		M_AXIS_TLAST	=> m00_axis_tlast,
		M_AXIS_TREADY	=> m00_axis_tready
	);
	
	SortingCore_inst : SortingCore
	generic map (
	    WORDS_NUM => C_AXIS_NUM_OF_PACKETS
	)
	port map (
		DATA_INPUT	=> DATA,
		DATA_OUTPUT	=> RESULT,
		clk	=> m00_axis_aclk,
		rst	=> m00_axis_aresetn
	);
	
end arch_imp;
