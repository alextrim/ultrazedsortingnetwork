library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.sorting.all;

entity SortingNetwork_v1_0_M00_AXIS is
	generic (
		-- Users to add parameters here
		C_M_AXIS_NUM_OF_PACKETS           : integer   := 8
		-- User parameters ends
		-- Do not modify the parameters beyond this line
	);
	port (
		-- Users to add ports here
        M_STREAM_DATA : in WORD_ARRAY_type(0 to (C_M_AXIS_NUM_OF_PACKETS)-1);
        M_NEW_DATA_READY : in std_logic;
        M_DATA_TRANSMITTED : out std_logic;
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global ports
		M_AXIS_ACLK	: in std_logic;
		-- 
		M_AXIS_ARESETN	: in std_logic;
		-- Master Stream Ports. TVALID indicates that the master is driving a valid transfer, A transfer takes place when both TVALID and TREADY are asserted. 
		M_AXIS_TVALID	: out std_logic;
		-- TDATA is the primary payload that is used to provide the data that is passing across the interface from the master.
		M_AXIS_TDATA	: out std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0);
		-- TSTRB is the byte qualifier that indicates whether the content of the associated byte of TDATA is processed as a data byte or a position byte.
		M_AXIS_TSTRB	: out std_logic_vector(((C_AXIS_TDATA_WIDTH)/8)-1 downto 0);
		-- TLAST indicates the boundary of a packet.
		M_AXIS_TLAST	: out std_logic;
		-- TREADY indicates that the slave can accept a transfer in the current cycle.
		M_AXIS_TREADY	: in std_logic
	);
end SortingNetwork_v1_0_M00_AXIS;

architecture implementation of SortingNetwork_v1_0_M00_AXIS is

	-- Define the states of state machine                                                                             
	type state is ( IDLE,
	                READY_TO_STREAM,                  
	                SEND_STREAM,
	                DONE);
	                     
	-- State variable                                                                 
	signal  mst_exec_state : state;                                                   
	-- Example design FIFO read pointer                                               
	signal read_pointer : integer range 0 to C_M_AXIS_NUM_OF_PACKETS;                               

	-- AXI Stream internal signals
	--streaming data valid
	signal axis_tvalid	: std_logic;
	
	signal axis_tvalid_delay	: std_logic;
	--Last of the streaming data 
	signal axis_tlast	: std_logic;
	--Last of the streaming data delayed by one clock cycle
	signal axis_tlast_delay	: std_logic;
	--FIFO implementation signals
	signal stream_data_out	: std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0);
	signal tx_en	: std_logic;
	--The master has issued all the streaming data stored in FIFO
	signal tx_done	: std_logic;


begin
	-- I/O Connections assignments

	M_AXIS_TVALID	<= axis_tvalid_delay;
	M_AXIS_TDATA	<= stream_data_out;
	M_AXIS_TLAST	<= axis_tlast_delay;
	M_AXIS_TSTRB	<= (others => '1');

	-- Control state machine implementation                                               
	process(M_AXIS_ACLK, M_NEW_DATA_READY)                                                                        
	begin                                                                                       
	  if (rising_edge (M_AXIS_ACLK)) then                                                       
	    if(M_AXIS_ARESETN = '0') then                                                           
	      -- Synchronous reset (active low)                                                     
	      mst_exec_state      <= IDLE;
	      M_DATA_TRANSMITTED  <= '0';                                                                                                          
	    else                                                                                    
	      case (mst_exec_state) is                                                              
	        when IDLE     =>
	            if ( M_NEW_DATA_READY = '1') then
	              mst_exec_state  <= READY_TO_STREAM;
	            else
	              mst_exec_state  <= IDLE;
	            end if;
	        
	        when READY_TO_STREAM =>
	          mst_exec_state  <= SEND_STREAM;
	          
	        when SEND_STREAM  =>                                      
	          if (tx_done = '1') then   
	            M_DATA_TRANSMITTED <= '1';                                                        
	            mst_exec_state <= DONE;
	          else                                                                              
	            mst_exec_state <= SEND_STREAM;                                                  
	          end if;    
	                                                                                 
	        when DONE =>
	           if(M_NEW_DATA_READY = '0') then
	               M_DATA_TRANSMITTED <= '0';
	               mst_exec_state <= IDLE;
	           end if;
	                                                                                     
	        when others    =>                                                                   
	          mst_exec_state <= IDLE;                                                           
	                                                                                            
	      end case;                                                                             
	    end if;                                                                                 
	  end if;                                                                                   
	end process;                                                                                


	--tvalid generation
	axis_tvalid <= '1' when ((mst_exec_state = SEND_STREAM) and (read_pointer < C_M_AXIS_NUM_OF_PACKETS)) else '0';
	                                                                                               
	-- AXI tlast generation                                                                                              
	axis_tlast <= '1' when (mst_exec_state = SEND_STREAM and read_pointer = C_M_AXIS_NUM_OF_PACKETS-1) else '0';                     
	                                                                                               
	-- Delay the axis_tvalid and axis_tlast signal by one clock cycle                              
	-- to match the latency of M_AXIS_TDATA                                                        
	process(M_AXIS_ACLK)                                                                           
	begin                                                                                          
	  if (rising_edge (M_AXIS_ACLK)) then                                                          
	    if(M_AXIS_ARESETN = '0') then                                                              
	      axis_tvalid_delay <= '0';                                                                
	      axis_tlast_delay <= '0';                                                                 
	    else                                                                                       
	      axis_tvalid_delay <= axis_tvalid;                                                        
	      axis_tlast_delay <= axis_tlast;                                                          
	    end if;                                                                                    
	  end if;                                                                                      
	end process;                                                                                   

	process(M_AXIS_ACLK)                                                       
	begin                                                                            
	  if (rising_edge (M_AXIS_ACLK)) then                                            
	    if(M_AXIS_ARESETN = '0' or mst_exec_state = IDLE) then                                                
	      read_pointer <= 0;                                                         
	      tx_done  <= '0';                                                           
	    else                                                                         
	      if (read_pointer < C_M_AXIS_NUM_OF_PACKETS) then                         
	        if (tx_en = '1') then                                                    
	          -- read pointer is incremented after every read from the FIFO          
	          -- when FIFO read signal is enabled.                                   
	          read_pointer <= read_pointer + 1;                                      
	          tx_done <= '0';                                                        
	        end if;                                                                  
	      elsif (read_pointer = C_M_AXIS_NUM_OF_PACKETS) then                         
	        -- tx_done is asserted when NUMBER_OF_OUTPUT_WORDS numbers of streaming data
	        -- has been out.                                                         
	        tx_done <= '1';                                                          
	      end  if;                                                                   
	    end  if;                                                                     
	  end  if;                                                                       
	end process;                                                                     


	--FIFO read enable generation 
	tx_en <= M_AXIS_TREADY and axis_tvalid;                                   
	                                                                                
	-- FIFO Implementation                                                          
	                                                                                
	-- Streaming output data is read from FIFO                                      
	  process(M_AXIS_ACLK)                                                                                                  
	  begin                                                                         
	    if (rising_edge (M_AXIS_ACLK)) then                                         
	      if(M_AXIS_ARESETN = '0' or mst_exec_state = IDLE) then                                             
            stream_data_out <= (others => '1');
	      elsif (tx_en = '1') then 
                stream_data_out(C_AXIS_TDATA_WIDTH-1 downto 0) <= M_STREAM_DATA(read_pointer);
	      end if;                                                                   
	     end if;                                                                    
	   end process;                                                                 

end implementation;
