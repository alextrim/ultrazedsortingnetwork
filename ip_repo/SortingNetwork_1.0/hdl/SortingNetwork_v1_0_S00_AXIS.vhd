library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.sorting.all;

entity SortingNetwork_v1_0_S00_AXIS is
	generic (
		C_S_AXIS_NUM_OF_PACKETS           : integer   := 8
	);
	port (
		-- Users to add ports here
        S_STREAM_DATA : out WORD_ARRAY_type(0 to (C_S_AXIS_NUM_OF_PACKETS)-1);
        S_NEW_DATA_READY : out std_logic;
        S_DATA_TRANSMITTED : in std_logic;
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- AXI4Stream sink: Clock
		S_AXIS_ACLK	: in std_logic;
		-- AXI4Stream sink: Reset
		S_AXIS_ARESETN	: in std_logic;
		-- Ready to accept data in
		S_AXIS_TREADY	: out std_logic;
		-- Data in
		S_AXIS_TDATA	: in std_logic_vector((C_AXIS_TDATA_WIDTH)-1 downto 0);
		-- Byte qualifier
		S_AXIS_TSTRB	: in std_logic_vector(((C_AXIS_TDATA_WIDTH)/8)-1 downto 0);
		-- Indicates boundary of last packet
		S_AXIS_TLAST	: in std_logic;
		-- Data is in valid
		S_AXIS_TVALID	: in std_logic
	);
end SortingNetwork_v1_0_S00_AXIS;

architecture arch_imp of SortingNetwork_v1_0_S00_AXIS is

	-- Define the states of state machine 
	type state is ( IDLE,
	                WRITE_FIFO,
	                PROCESSING);
	                
	signal axis_tready	: std_logic;
	signal tlast_delay : std_logic;
	signal  mst_exec_state : state;
	
	signal rx_en : std_logic;
	signal rx_done : std_logic;
	
	signal reg_full : std_logic;
	signal reg_reset : std_logic;
	
	component ShiftReg is
        generic (
            depth : integer   := 8
        );
        port (
            clk : in std_logic;
         
            resetn : in std_logic;
            en : in std_logic;
            full : out std_logic;
            
            data_in : in std_logic_vector(C_AXIS_TDATA_WIDTH-1 downto 0);
            dataArray : out WORD_ARRAY_type(0 to depth-1)
        );
    end component;
	
begin

	REG : ShiftReg generic map (C_S_AXIS_NUM_OF_PACKETS) port map (
	   clk => S_AXIS_ACLK,
	   resetn => reg_reset,
	   en => rx_en,
	   full => reg_full,
	   data_in => S_AXIS_TDATA,
	   dataArray => S_STREAM_DATA);

    axis_tready <= '1' when rx_done = '0' and mst_exec_state = WRITE_FIFO else '0';
	S_AXIS_TREADY	<= axis_tready;
    
    rx_en <= S_AXIS_TVALID and axis_tready;
    rx_done <= reg_full or tlast_delay;
    
    state_machine: process(S_AXIS_ACLK)
	begin
	  if (rising_edge (S_AXIS_ACLK)) then
	    if(S_AXIS_ARESETN = '0') then
	      mst_exec_state      <= IDLE;
	      S_NEW_DATA_READY    <= '0';
	      reg_reset <= '0';
	    else
	      case (mst_exec_state) is
	        when IDLE     => 
	          if (S_AXIS_TVALID = '1' and S_DATA_TRANSMITTED = '0')then
	            mst_exec_state <= WRITE_FIFO;
	            reg_reset <= '1';
	          else
	            reg_reset <= '0';
	          end if;
	        when WRITE_FIFO =>
              if (rx_done = '1') then   
	            S_NEW_DATA_READY <= '1';                                               
	            mst_exec_state <= PROCESSING;                                               
	          end if;   
	        when PROCESSING =>
	           if(S_DATA_TRANSMITTED = '1') then
	               S_NEW_DATA_READY <= '0';
	               mst_exec_state <= IDLE;
	           end if;
	        when others    => 
	          mst_exec_state <= IDLE;
	      end case;
	    end if;  
	  end if;
	end process;
	
	tlast_delay_process: process(S_AXIS_ACLK)
	begin
	   if (rising_edge (S_AXIS_ACLK)) then
           if(axis_tready = '1' and S_AXIS_TLAST = '1') then
	           tlast_delay <= '1';
	       else
	           tlast_delay <= '0';
	       end if;
	   end if;
	end process;

end arch_imp;
