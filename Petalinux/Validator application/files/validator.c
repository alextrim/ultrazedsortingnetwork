/*
* Copyright (C) 2013 - 2016  Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without restriction,
* including without limitation the rights to use, copy, modify, merge,
* publish, distribute, sublicense, and/or sell copies of the Software,
* and to permit persons to whom the Software is furnished to do so,
* subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in this
* Software without prior written authorization from Xilinx.
*
*/

#include <stdio.h>

int main(int argc, char **argv)
{
    //argc = 2;
    //argv[1] = "test3.hex";

    printf("DEBUG: argc=%d | sizeof(unsigned int) = %d\n", argc, sizeof(unsigned int));

    if(sizeof(unsigned int) != 4){
        printf("unsigned int size not equals 4 byte!");
        return 1;
    }

	if(argc != 2)
	{
		printf("no input file!\n");
		return 1;
	}

	FILE *fp;
	unsigned int buf;
	unsigned int buf2;

	if ((fp = fopen(argv[1], "rb")) == NULL)
	{
	printf("Unable to open file: %s\n", argv[1]);
	return 2;
	}

	if(fread(&buf, 4, 1, fp) != 1){
	printf("Unable read first element!\n");
	return 3;
	}

	while(fread(&buf2, 4, 1, fp) == 1){
		printf("%08x > %08x? ", buf, buf2);
		if(buf > buf2){
	    printf("ERROR!\n");
	    //break;
		}else{
		    printf("OK\n");
		}
		buf = buf2;
	}

	fclose(fp);
	return 0;
}
